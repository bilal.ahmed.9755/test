from django.contrib import admin
from .models import User, Slot, Schedule, Subject, Fee

# Register your models here.
admin.site.register(Fee)
admin.site.register(Subject)
admin.site.register(User)
admin.site.register(Slot)
admin.site.register(Schedule)
