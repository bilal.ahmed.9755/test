from django.urls import path
from . import views

urlpatterns = [
    path('check_cancelled/<str:student_name>/<str:teacher_name>/', views.check_cancelled.as_view(),
         name="check_cancelled"),

    path('check_reschedule/<str:student_name>/', views.check_reschedule.as_view(), name="check_reschedule"),

    path('available_student_slots/<str:student_name>/', views.available_student_slots.as_view(),
         name='available_student_slots'),

    path('available_teacher_slots/<str:student_name>/<str:teacher_name>/', views.available_teacher_slots.as_view(),
         name='available_teacher_slots'),

    path('insert_schedule/', views.insert_schedule.as_view(), name='insert_schedule'),

    path('reschedule/<int:pk>/', views.reschedule.as_view(), name='reschedule'),

    path('show_fee/<str:student_name>/', views.show_fee.as_view(), name='show_fee'),

    path('insert_fee/', views.insert_fee.as_view(), name='insert_fee'),

    path('update_fee/<int:record_id>/', views.update_fee.as_view(), name='update_fee'),
]
