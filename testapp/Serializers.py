from rest_framework import serializers
from .models import Schedule, User, Slot, Fee, Subject


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ['class_name', 'date', 'student_name', 'teacher_name', 'slot', 'updated', 'is_cancelled']


class SlotSerializer(serializers.ModelSerializer):
    class Meta:
        model = Slot
        fields = ['start_time']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username']


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = ['subject_name']


class FeeSerializer(serializers.ModelSerializer):

    student = UserSerializer(many=False, read_only=True)
    subject = SubjectSerializer(many=False, read_only=True)

    class Meta:
        model = Fee
        fields = ['custom_fee', 'subject', 'student']
