from .models import Schedule, Slot, Fee
from .Serializers import SlotSerializer, ScheduleSerializer, FeeSerializer
from datetime import datetime
from rest_framework.views import APIView
from rest_framework.response import Response


# Create your views here.
def show_cancelled_slots(name):
    today = datetime.now().date()
    current_time = datetime.now().time()
    cancelled_slots = Schedule.objects.filter(teacher_name__username=name, date=today,
                                              slot__start_time__gt=current_time,
                                              is_cancelled=True).values('slot__start_time')
    return cancelled_slots


def show_empty_slots(name):
    # reusable for both student and teacher
    today = datetime.now().date()
    current_time = datetime.now().time()
    scheduled_classes = Schedule.objects.filter(student_name__username=name, date=today,
                                                slot__start_time__gt=current_time).values('slot__start_time')
    all_slots = Slot.objects.filter(start_time__gt=current_time).values('start_time')
    empty_slots = all_slots.difference(scheduled_classes)
    return empty_slots


def get_fee(student_name):
    # getting student_name, custom_fee and subject_name
    query_set = Fee.objects.filter(student__username=student_name)
    return query_set


class available_student_slots(APIView):

    # selecting all future scheduled slots
    # selecting all future slots
    # taking difference

    @staticmethod
    def get(request, student_name):
        results = show_empty_slots(student_name)
        serial_obj = SlotSerializer(results, many=True)
        return Response(serial_obj.data)


class available_teacher_slots(APIView):
    @staticmethod
    def get(request, student_name, teacher_name):
        # getting all available slots of student
        empty_student_slots = show_empty_slots(student_name)
        # getting all available slots of teacher
        empty_teacher_slots = show_empty_slots(teacher_name)
        # checking the overlapping between records
        common_slots = empty_student_slots.intersection(empty_teacher_slots)
        serial_obj = SlotSerializer(common_slots, many=True)
        return Response(serial_obj.data)


class check_reschedule(APIView):

    @staticmethod
    def get(request, student_name):
        # It will show the future record which is not rescheduled...
        current_time = datetime.now().time()
        query_set = Schedule.objects.filter(updated=False, student_name__username=student_name,
                                            slot__start_time__gt=current_time)
        serial_obj = ScheduleSerializer(query_set, many=True)
        return Response(serial_obj.data)


class check_cancelled(APIView):

    # Checking cancelled slots of specific teacher and finding common empty slots of that teacher and student...
    @staticmethod
    def get(request, student_name, teacher_name):
        teacher_cancelled_classes = show_cancelled_slots(teacher_name)
        student_empty_classes = show_empty_slots(student_name)
        query_set = student_empty_classes.intersection(teacher_cancelled_classes)
        serial_obj = SlotSerializer(query_set, many=True)
        return Response(serial_obj.data)


class insert_schedule(APIView):
    @staticmethod
    def post(request):

        serial_obj = ScheduleSerializer(data=request.data)
        if serial_obj.is_valid():
            serial_obj.save()
            return Response(serial_obj.data)
        else:
            return Response(status=400)


class reschedule(APIView):
    @staticmethod
    def put(request, pk):
        old_record = Schedule.objects.get(id=pk)
        if old_record.updated:
            # bad request code
            return Response(status=400)
        else:
            serial_obj = ScheduleSerializer(old_record, data=request.data)
            if serial_obj.is_valid():
                serial_obj.save()
                return Response(serial_obj.data)
            else:
                return Response(status=422)

    @staticmethod
    def get(request, pk):
        query_set = Schedule.objects.get(id=pk)
        serial_obj = ScheduleSerializer(query_set)
        return Response(serial_obj.data)


class show_fee(APIView):
    @staticmethod
    def get(request, student_name):

        query_set = get_fee(student_name)
        serial_obj = FeeSerializer(query_set, many=True)
        return Response(serial_obj.data)


class insert_fee(APIView):

    @staticmethod
    def post(request):
        serial_obj = FeeSerializer(data=request.data)
        if serial_obj.is_valid():
            serial_obj.save()
            return Response(serial_obj.data)
        else:
            return Response(status=400)


class update_fee(APIView):

    @staticmethod
    def get(request, record_id):
        query_set = Fee.objects.get(id=record_id)
        serial_obj = FeeSerializer(query_set)
        return Response(serial_obj.data)

    @staticmethod
    def put(request, record_id):
        old_record = Fee.objects.get(id=record_id)
        serial_obj = FeeSerializer(old_record, data=request.data)
        if serial_obj.is_valid():
            serial_obj.save()
            return Response(serial_obj.data)
        else:
            return Response(status=400)
