from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.


class User(AbstractUser):
    TYPE = {
        ("Student", "Student"),
        ("Teacher", "Teacher")
    }
    type = models.CharField(max_length=25, choices=TYPE)


class Slot(models.Model):
    start_time = models.TimeField()
    end_time = models.TimeField()


class Schedule(models.Model):
    class_name = models.CharField(max_length=25)
    student_name = models.ForeignKey(User, related_name='student', on_delete=models.CASCADE)
    teacher_name = models.ForeignKey(User, related_name='teacher', on_delete=models.CASCADE)
    date = models.DateField(null=True)
    slot = models.ForeignKey(Slot, on_delete=models.CASCADE)
    updated = models.BooleanField(null=True, blank=True)
    is_cancelled = models.BooleanField(null=True, blank=True)

    def __str__(self):
        return self.class_name


class Subject(models.Model):
    subject_name = models.CharField(max_length=25)
    default_fee = models.IntegerField()

    def __str__(self):
        return self.subject_name


class Fee(models.Model):
    custom_fee = models.IntegerField()
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    student = models.ForeignKey(User, on_delete=models.CASCADE)
